const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const Players = new Schema ({
        name: { type: String, required: true },
        jersyNumber: { type: Number, required: true },
});

module.exports = mongoose.model('Players', Players)
