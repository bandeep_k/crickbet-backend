var http = require('http'); // 1 - Import Node.js core module
const mongoose = require('mongoose');
const db = require('./db');
const path = require('path');
const Players = require('./models/cricketers');
const allPlayers = require('./models/constants.cricketers');
// const path = __dirname + '/views/';


var server = http.createServer(function (req, res) {   // 2 - creating server
  console.log('\n\n\n',req.headers);

  if (req.url == "/addPlayerList") {
    try{
      res.writeHead(200, { 'Content-Type': 'text/html' });
      for(let allPlayersIdx = 0; allPlayersIdx < allPlayers.players.length;allPlayersIdx++){
        let player = new Players({name:allPlayers.players[allPlayersIdx].name,jersyNumber:allPlayers.players[allPlayersIdx].jersyNumber});
        player.save();
      }
      res.write('<html><body><p>This is student Page.</p></body></html>');
      res.end();
    }
    catch(err){
      console.error(allPlayers.players);
      console.error("Server Error")
    }
  }
  else if (req.url == "/showPlayers"){
    res.setHeader('Content-Type', 'application/json');
      let myFirstPromise = new Promise((resolve, reject) => {
      let allPlayers = Players.find({}).exec();
      setTimeout( function() {
        resolve("Success!")  // Yay! Everything went well!
      }, 250)
    })

    myFirstPromise.then((successMessage) => {
      res.status = 200;
      res.end(JSON.stringify({data:allPlayers}));
    });
  }

});

server.listen(5000); //3 - listen for any incoming requests

console.log('Node.js web server at port 5000 is running..')
